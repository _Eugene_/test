<?php

namespace App\Models;

use Carbon\Carbon;
use GuzzleHttp\Psr7\Request;
use Illuminate\Database\Eloquent\Model;
use App\Models\Position;
use DB;


class Employee extends Model {
    public $attributes = ['boss_id' => 0];
    protected $fillable = [
        'name',
        'salary',
        'position',
        'boss_id',
    ];
    protected $dates = [
        'created_at',
        'updated_at',
    ];
    protected $guarded = [
        'id',
        'created_at',
        'updated_at',
    ];
    public $table = 'employee';

    public function scopeFilter($builder, $request) {

        if ($request->has('name')) {
            $builder->where('name', 'like', '%' . $request->name . '%');
        }
        if ($request->has('position')) {
            $builder->where('employee.position', $request->position);
        }

        if ($request->has('created_at')) {
            $builder->where(
                DB::raw('date_format(created_at, "%Y-%m-%d")') , $request->created_at) ;   

        }


        if ($request->has('orderBy')) {


            $orderBy = json_decode($request->get('orderBy'), 1);

            foreach ($orderBy as $key => $value) {
                if($value){
                    if($key == "position") {
                        $builder->orderBy('positions.' . $key, $value == 1 ? 'asc' : 'desc');
                    } else {
                        $builder->orderBy($key, $value == 1 ? 'asc' : 'desc');
                    }
                }
            }
        }


        return $builder;
    }

    public function children() {
        return $this->hasMany('App\Models\Employee', 'boss_id', 'id');
    }


    public function getBoss() {
        return $this->hasOne('App\Models\Employee', 'id', 'boss_id');
    }


    public function relPosition() {
        return $this->hasOne(Position::class, 'id', 'position');
    }

    public function childrenWithPosition() {
        return $this->children()->with('relPosition');
    }

    public static function getChildren() {
//        dd('test');
        return
            self::leftJoin(
                DB::raw(
                    '(' .
                    Employee::select(
                        DB::raw('boss_id as boss, count(id) as childrenCount'))
                        ->groupBy('boss_id')
                        ->toSql() .
                    ') as rel'),
                'rel.boss', 'employee.id');
    }

    public function deleteWithSaveIerarchy() {

        if ($this->children->count()) {
            $this->children()->update(['boss_id' => $this->boss_id]);
        }
        $children = $this->getRelation('children');

        $this->delete();

        return $children;

    }
}
