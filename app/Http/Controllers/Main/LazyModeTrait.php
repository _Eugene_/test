<?php
namespace App\Http\Controllers\Main;

trait LazyModeTrait{
    public function getTree($data){
//    dd($data);
        foreach($data as $key => $value){

            $arr[$value->boss_id][] = $value;
        }


        return $this->buildTreeHtml($arr);
    }

    function buildTreeHtml($data, $parent = 0){

        $html = '<ul '. (!$parent ? 'class="tree"' : 'class="display-none"') . '>';
        foreach($data[$parent] as $key => $value){

            $itemIsBoss = isset($data[$value->id]) ;

            $html .= '<li class="'. ($itemIsBoss ? 'branch' : '') .'">' ;
            $html .= '<div class="item">';
            if($itemIsBoss  ){
                $html .= '<i class="indicator glyphicon glyphicon-chevron-right"></i>';
            }
            $html .= '<a href="#" data-id="'. $value->id.'" data-cnt="'. ($itemIsBoss ? 1 : 0 ) .'">'. $value->name;
            $html .= '<span class="position">('. $value->getRelation('relPosition')->position .')</span>';
            $html .= '</a>';
            $html .= '</div>';

            if($itemIsBoss){

                $html .= $this->buildTreeHtml($data, $value->id);
            } else {
                $html .= '<ul class="display-none"></ul>';
            }



            $html .= '</li>';
        }
        $html .= '</ul>';
        return $html;
    }
    function buildTree($data, $parent = 0){
        foreach($data[$parent] as $key => $value){

            $arr =
                [
                    'name' => $value->name,
                    'id' => $value->id,
                    'boss' => $value->boss_id,
                ];

            if($data[$value->id]){
                $arr['childern'] = $this->buildTree($data, $value->id);
            }

            $result[] = $arr;

        }
        return $result;
    }




}
