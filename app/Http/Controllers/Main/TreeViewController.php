<?php

namespace App\Http\Controllers\Main;

use App\Http\Utils\BaseController;
use App\Models\Employee;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\Http\Controllers\Main\LazyModeTrait as LazyModeTrait;

class TreeViewController extends BaseController {
    use LazyModeTrait;
    public function actionIndex () {

        $employee = $this->getEmployee();

        return $this->view("tree", [
            'employee' => $employee,
        ]);
    }

    public function actionAjaxGetChildren (Request $request) {

        $employee = $this->getEmployee($request->get('id'));

        return view($this->viewPath . '.treeContent', ['employee' => $employee]);
    }

    public function actionLazy(){
        $html = $this->getTree(Employee::orderBy('boss_id')->with('relPosition')->get());
        return $this->view('tree',['lazy' => 1, 'html' => $html]);
    }


    private function getEmployee ($parent = 0) {
        return Employee::getChildren()->with('relPosition')
            ->where('boss_id', $parent)->get();

    }

    public function actionAjaxDeleteItem(Request $request){
        $employee = Employee::find($request->id);

        $children = $employee->deleteWithSaveIerarchy();

        return view($this->viewPath . '.treeContent', ['employee' => $children]);
    }

    public function actionAjaxUpdateIerarchy(Request $request){
        $employee = Employee::find($request->id);
        $employee->boss_id = $request->newBoss;
        $employee->save();
    }


}
