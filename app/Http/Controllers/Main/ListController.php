<?php

namespace App\Http\Controllers\Main;

use App\Http\Utils\BaseController;
use App\Models\Employee;
use App\Models\Position;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class ListController extends BaseController {

    public function __construct() {
        $this->middleware('auth');
        parent::__construct();
    }

    public function actionIndex(Request $request) {


//        dd($request->get('position'));
        $data = $this->getList($request);


        $data['modal'] = $request->ajax();

        if($request->ajax()){
            return view($this->viewPath . '.listEmployee',
                $data
            );
        }

        return $this->view('listEmployee',
            $data);
    }

    public function actionAjaxList(Request $request) {


        return view($this->viewPath . '.employeeTable', $this->getList($request));

    }

    private function getList($request) {

        $employee = Employee::filter($request)
            ->select(DB::raw('employee.*, positions.position'))
            ->with('relPosition')
            ->leftJoin('positions', 'employee.position', 'positions.id');
        if($request->get('selectBoss')){
            $employee = $employee->where('employee.id', '!=', $request->id);
        }

        $employee = $employee->paginate(10);
        $positions = Position::pluck('position', 'id');


        return [
            'order' => json_decode($request->get('orderBy'),1),
            'employee' => $employee,
            'position' => $positions,
            'modal' => $request->modal,
        ];
    }


}
