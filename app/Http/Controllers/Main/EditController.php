<?php

namespace App\Http\Controllers\Main;

use App\Http\Requests\EditEmployee;
use App\Http\Utils\BaseController;
use App\Models\Employee;
use App\Models\Position;
use GuzzleHttp\Psr7\Response;
use Image;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

class EditController extends BaseController {

    public function __construct() {
        parent::__construct();
        $this->middleware('auth');
    }

    public function actionIndex(EditEmployee $request) {

//        dd(preg_match('/^[0-9]{1,10}((\.{1}[0-9]{1,2})|\.{0})/','1.5514444'));

        if ($request->id) {
            $employee = Employee::with('getBoss')->find($request->id);

        } else {
            $employee = new Employee();
        }

        if($this->saveData($request, $employee)){
            return redirect('/list');
        }

        return $this->view('index', [
            'employee'  => $employee,
            'positions' => [0 => 'Please Select'] + Position::pluck('position', 'id')->toArray(),
        ]);
    }

    private function saveData($request, $employee) {

        if($request->save) {

            $data = $request->all();

            $employee->fill($request->all());

            if($request->deleteImg) {
                $employee->photo = '';

            } else {
                if(Input::file('photo')) {

                    $img = Image::make(Input::file('photo')->getRealPath())->save(app()->basePath() . '/public/img/'.md5(microtime()) . '.' . Input::file('photo')->getClientOriginalExtension());
                    $employee->photo = $img->basename;
                }
            }

            $employee->save();

            return true;

        }
        return false;
    }

    public function actionAjaxDelete(Request $request){
        $employee = Employee::find($request->id);
        $employee->deleteWithSaveIerarchy();
    }
}
