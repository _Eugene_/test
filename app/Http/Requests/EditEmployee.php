<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class EditEmployee extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

//        dd($this->position);
        if($this->save) {
            return [
                'name'   => 'required|max:100|min:5',
                'salary' => 'required|numeric|between:0, 99999999.99',
                'photo' => 'image',
                'boss_id' => [
                    Rule::notIn($this->id),
                ],
                'position' => 'exists:positions,positions.id',
            ];
        }
        return [];
    }

//    public function messages(){
//
//    }
}
