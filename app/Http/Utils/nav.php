<?php

namespace App\Http\Utils;

return [
    [
        'url' => '/',
        'name' => 'treeView',
    ],
    [
        'url' => '/treeView/lazy',
        'name' => 'lazyTree',
    ],
    [
        'url' => '/list',
        'name' => 'list',
    ],

];