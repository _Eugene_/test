<?php

namespace App\Http\Utils;

use Illuminate\Http\Request;

class UrlHelper {
    public function getActionByUrl(Request $request){
        $url = substr($request->getPathInfo(),1);

        $controllerPath = 'Main\\';
        $controller = 'TreeViewController';
        $action = 'actionIndex';
        
        if(!empty($url)){
            $urlArray = explode('/',$url);
            $controller = ucfirst($urlArray[0]) . 'Controller';
            $action = $urlArray[1] ? 'action' . ucfirst($urlArray[1]): $action;
        }

        return $controllerPath . $controller . '@' . $action;

    }
}