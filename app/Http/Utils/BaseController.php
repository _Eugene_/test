<?php

namespace App\Http\Utils;

use Request as Req;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use App\Http\Utils\nav;
class BaseController extends Controller
{

    protected $viewPath;

    public function __construct() {
        $classNameArr = explode('\\', str_replace('Controller','',get_class($this)));
        $this->viewPath = camel_case($classNameArr[count($classNameArr) -1 ]);
        View::share('viewPath', $this->viewPath);
    }

    protected function view($viewName, $params = []){

        return view(
            'layouts.layout',
            [
                'nav' => $this->getMenu(),
                'content' => view($this->viewPath . '.' . $viewName, $params)
            ]
            );
    }

    private function getMenu(){

        $nav = require_once app()->basePath().'/app/Http/Utils/nav.php';
        return view('layouts.nav', ['items' => $nav, 'path' => (Req::path() == '/' ? Req::path() : '/'. Req::path())]);
    }
}
