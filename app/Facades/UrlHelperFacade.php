<?php
namespace App\Facades;

use \Illuminate\Support\Facades\Facade;
class UrlHelperFacade extends Facade {

    protected static function getFacadeAccessor () {
        return 'urlHelper';
    }

}