<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Http\Utils\UrlHelper;

class UrlHelperProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('urlHelper', function ($app) {
            return new UrlHelper();
        });
    }
}
