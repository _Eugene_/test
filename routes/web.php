<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();
Route::any('/{controller?}/{action?}', app()->call([app('urlHelper'), 'getActionByUrl']));
//Route::get('/', 'Main\EmployeeTree@actionIndex')->middleware('auth');
//Route::any('/getChildren', 'Main\EmployeeTree@actionAjaxGetChildren')->middleware('auth');
//Route::any('/list', 'Main\ListController@actionIndex')->middleware('auth');
//Route::get('/', function () {
//    return view('welcome');
//});



Route::get('/home', 'HomeController@index')->name('home');
