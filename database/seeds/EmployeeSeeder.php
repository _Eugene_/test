<?php

use Illuminate\Database\Seeder;

class EmployeeSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */

    private $level;
    private $maxChildItem;
    public function run () {


        $this->level = $this->command->ask('Укажите максимальный уровень вложенности дерева',6);
        $this->maxChildItem = $this->command->ask('Укажите максимальное количество дочерних узлов',6);

        $maxCount = 0;
        for($i = 1; $i <= $this->level; $i++){
            $maxCount += pow($this->maxChildItem,$i);
        }

        if( $maxCount > 5000) {
            $question = $this->command->ask('В БД будет добавлено ~' . $maxCount . ' записей. Для продолжение нажмите "Y"');

            if(strtoupper($question) != 'Y') {
                die();
            }
        }

        DB::table('employee')->truncate();
        $this->addChildren(null,0);

        $count = \App\Models\Employee::count();

        $this->command->info('Заполнение закочено. К-во записе: ' .$count);

    }

    private function addChildren ($parent = null, $level = 0) {

        if($level >= $this->level) {
            return false;
        }
//        $level += rand(1,  3);
        $level ++ ;


//        $children = factory(App\Models\Employee::class, 'boss', rand(ceil($this->maxChildItem * 0.6), $this->maxChildItem))->create()->each(function ($q) use ($level) {
        $children = factory(App\Models\Employee::class, 'boss', $this->maxChildItem)->create()->each(function ($q) use ($level) {
            $this->addChildren($q, $level);
        });

        if(!is_null($parent)) {
                $parent->children()->saveMany($children);
        }

    }
}
