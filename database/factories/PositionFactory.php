<?php

$factory->defineAs(App\Models\Position::class, 'position', function (Faker\Generator $faker) {

    return [

        'position' => 'position_' . $faker->unique()->randomDigit(),

    ];
});