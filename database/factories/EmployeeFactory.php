<?php

$factory->defineAs(App\Models\Employee::class, 'boss', function (Faker\Generator $faker) {

    return [
        'name'     => $faker->name,
        'position' => $faker->randomElement(App\Models\Position::pluck('id')->toArray()),
        'salary'   => $faker->randomFloat(2,1000,10000),

    ];
});

//
//$factory->defineAs(App\Models\Employee::class,'child', function (Faker\Generator $faker){
//    return [
//        'name' => $faker->name,
//        'position' => 1,
//        'salary' => $faker->randomFloat(10,2),
//        'is_boss' => 0,
//
//    ];
//});