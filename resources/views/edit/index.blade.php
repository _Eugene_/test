<div class="row">
    <div class="panel panel-primary">
        <div class="panel-heading">
            header
        </div>
        <div class="panel-body">
            {{ Form::open(['url' => Request::getRequestUri(),'files' => true]) }}
            <div class="col-md-6">
                <div class="form-group @if($errors->has('name')) has-error @endif">
                    {{ Form::label('name','name',['class' => 'control-label' ]) }}
                    {{ Form::text('name', $employee->name ,['class' => 'form-control']) }}
                    <div class="erorr-message-form">
                        @if($errors->has('name'))
                            <div class="help-block custom">{{ $errors->first('name') }}</div>
                        @endif
                    </div>
                </div>

                <div class="form-group @if($errors->has('salary')) has-error @endif">
                    {{ Form::label('salary','salary',['class' => 'control-label' ]) }}
                    {{ Form::number('salary', $employee->salary ,['class' => 'form-control', 'step' => 0.0000001]) }}
                    <div class="erorr-message-form">
                        @if($errors->has('salary'))
                            <div class="help-block custom">{{ $errors->first('salary') }}</div>
                        @endif
                    </div>
                </div>

                <div class="form-group @if($errors->has('position')) has-error @endif">

                    {{ Form::label('position','position',['class' => 'control-label' ]) }}
                    {{ Form::select('position', $positions ,$employee->position,['class' => 'select2']) }}
                    <div class="erorr-message-form">
                        @if($errors->has('position'))
                            <div class="help-block custom">{{ $errors->first('position') }}</div>
                        @endif
                    </div>
                </div>

                <div class="form-group @if($errors->has('boss_id')) has-error @endif">
                    {{ Form::label('boss') }}

                    <div class="input-group">
                        {{ Form::text('boss_name', $employee['relations']['getBoss']->name, ['class' => 'form-control', 'disabled' => true, 'id' => 'boss']) }}
                        {{ Form::hidden('boss_id', $employee->boss_id) }}
                        <span class="input-group-btn">
                        <button class="btn btn-primary" type="button" data-toggle="modal"
                                data-target="#select-boss">...</button>
                      </span>
                    </div>
                    <div class="erorr-message-form">
                        @if($errors->has('boss_id'))
                            <div class="help-block custom">{{ $errors->first('boss_id') }}</div>
                        @endif
                    </div>

                </div>

            </div>

            <div class="col-md-6">
                <div class="avatar text-center">
                    <div class="profile-avatar">
                        <div class="img-wrapper avatar-circle img-with-footer">

                            <img class="profile-avatar img-circle
                                    @if($errors->has('photo'))
                                    error-photo
                                    @endif
                                    "
                                     src=
                                     @if($employee->photo)
                                             "/img/{{ $employee->photo }}"
                                    @else
                                        "/img/default_user.jpg"
                                    @endif
                                >

                            <div class="img-footer display-none text-center">
                                <div class="btn-wrapper-img">
                                    <div class="img-edit edit-avatar"><i class="glyphicon glyphicon-edit"></i></div>
                                    <div class="img-delete delete-avatar"><i class="glyphicon glyphicon-trash"></i>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    @if($errors->has('photo'))
                        <div class="erorr-message-form has-error">
                            <div class="help-block custom">{{ $errors->first('photo') }}</div>
                        </div>
                    @endif
                    {{ Form::hidden('deleteImg') }}
                    {{ Form::file('photo', ['class' => 'display-none']) }}

                    {{--<div class="btn btn-danger delete-avatar">--}}
                    {{--<i class="fa fa-minus"></i>--}}
                    {{--</div>--}}
                    {{--<div class="btn btn-warning">--}}
                    {{--<i class="fa fa-edit"></i>--}}
                    {{--</div>--}}

                </div>
            </div>
            <div class="clearfix"></div>
            <div class="btn-wrapper text-right">
                {{ Form::hidden('id', $employee->id) }}
                {{ Form::input('submit', 'save', 'сохранить', ['class' => 'btn btn-primary']) }}
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>


@include('edit.selectBoss')
