
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading">
                tree
            </div>
            <div class="wrapper tree-wrapper">
                <div class="delete-tree-item-wrapper">
                    <div class="text-center delete-tree-item  display-none "><i class="glyphicon glyphicon-trash"></i></div>
                </div>
                {{--<div class="text-center delete-tree-item"><i class="glyphicon glyphicon-trash"></i></div>--}}

                @if(!$lazy)
                <ul class="tree">
                    @include($viewPath . '.treeContent')
                </ul>
                @else
                    @include($viewPath . '.lazyTree')
                @endif
            </div>

        </div>
    </div>