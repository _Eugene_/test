@foreach($employee as $key => $value)
    <li class="@if($value->childrenCount > 0){{'branch'}}@endif">

        {{--<i class="indicator glyphicon glyphicon-chevron-down"></i>--}}
        <div class="item">
            @if($value->childrenCount > 0)
                <i class="indicator glyphicon glyphicon-chevron-right"></i>
            @endif
            <a href="#" data-id="{{ $value->id }}" data-cnt="{{ $value->childrenCount ? $value->childrenCount : 0 }}">{{ $value->name }} <span class="position">({{ $value->getRelation('relPosition')->position }})</span></a>
        </div>

        {{--    @if($value->childrenCount > 0)--}}
        <ul class="display-none">

        </ul>
        {{--@endif--}}
    </li>

@endforeach
