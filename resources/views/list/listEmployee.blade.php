
    <div class="row">
        <div class="panel panel-primary">
            <div class="panel-heading">
                Filter
            </div>
            <div class="panel-body">
                {{ Form::open(['name' => 'filter']) }}
                {{ csrf_field() }}

                <div class="row">
                    <div class="col-md-4">
                        {{ Form::text('name', '', ['class' => 'form-control', 'placeholder' => 'имя'] ) }}
                    </div>
                    <div class="col-md-4">
                        {{ Form::select('position', $position,'',['class' => 'select2', 'placeholder' => 'должность'] ) }}
                    </div>
                    <div class="col-md-4">
                        {{ Form::date('created_at', '', ['class' => 'form-control', 'placeholder' => 'position'] ) }}
                    </div>
                </div>
                <div class="btn-wrapper text-right">
                    {{ Form::input('submit', 'search','find', ['class' => 'btn btn-primary']) }}
                </div>
                {{ Form::hidden('modal', $modal) }}
                {{ Form::hidden('page') }}
                {{ Form::close() }}
            </div>
        </div>
    </div>
    <div class="row">
        <div class="table-wrapper">
            @include('list.employeeTable')
        </div>

    </div>



