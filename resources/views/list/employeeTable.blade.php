<div class="panel panel-primary">

    <div class="panel-heading">
        <div class="row">
            <div class="col-md-6">Employee</div>
            <div class="col-md-6 text-right"><a class="right btn btn-primary" href="/edit">new</a></div>
        </div>


    </div>
    <table class="table table-hover">
        <tr>
            <th><span data-col-name="name" data-order="{{ $order['name'] ? $order['name'] : 0 }}"
                      class="glyphicon {{ $order['name'] ? ($order['name'] == 1 ? "glyphicon-sort-by-order" : "glyphicon-sort-by-order-alt") : ''}}"></span>name
            </th>
            <th><span data-col-name="position" data-order="{{ $order['position'] ? $order['position'] : 0}}"
                      class="glyphicon {{ $order['position'] ? ($order['position'] == 1 ? "glyphicon-sort-by-order" : "glyphicon-sort-by-order-alt") : ''}}"></span>position
            </th>
            <th><span data-col-name="salary" data-order="{{ $order['salary'] ? $order['salary'] : 0 }}"
                      class="glyphicon {{ $order['salary'] ? ($order['salary'] == 1 ? "glyphicon-sort-by-order" : "glyphicon-sort-by-order-alt") : ''}}"></span>salary
            </th>
            <th><span data-col-name="created_at" data-order="{{ $order['created_at'] ? $order['created_at'] : 0 }}"
                      class="glyphicon {{ $order['created_at'] ? ($order['created_at'] == 1 ? "glyphicon-sort-by-order" : "glyphicon-sort-by-order-alt") : ''}}"></span>created_at
            </th>
            <th>photo</th>
            <th></th>
        </tr>
        @foreach($employee as $value)
            <tr>
                <td>{{ $value->name }}</td>
                <td>{{ $value->position }}</td>
                <td>{{ $value->salary }}</td>
                <td>{{ $value->created_at }}</td>
                <td class="text-center">
                    <img src=
                         @if($value->photo)
                                 "/img/{{$value->photo}}"
                        @else
                            "/img/default_user.jpg"
                        @endif alt="" style="width:50px">
                </td>
                <td class="text-right">
                    <div class="text-right" style="display:inline-block;">
                        <a href="/edit?id={{ $value->id }}" class="btn btn-primary"> edit</a>
                        <a href="" data-id="{{ $value->id }}" class="btn btn-danger delete-employee"> delete</a>
                        @if($modal)
                            <button class="btn btn-primary accept-select-boss" data-id="{{ $value->id }}"
                                    data-name="{{ $value->name }}"><i class="glyphicon glyphicon-ok"></i></button>
                        @endif
                    </div>

                    {{--<div class="btn btn-primary">edit</div>--}}
                    {{--<div class="btn btn-danger">delete</div>--}}
                </td>
            </tr>
        @endforeach
    </table>

</div>
<div class="pagination-wrapper list-navigate text-right">
    {{ $employee->links() }}
</div>

