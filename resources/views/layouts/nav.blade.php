<nav class="navbar navbar-default">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/">Test of Knowleges</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                @foreach($items as $key => $item)
                    <li @if(($path ) == $item['url'])class="active" @endif><a href="{{ $item['url'] }}">{{ $item['name'] }}</a></li>
                @endforeach
            </ul>

            @if(!Auth::guest())
            <ul class="nav navbar-nav navbar-right btn-danger">
                <li><a href="#" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">sign out</a></li>
            </ul>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
                @endif
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>