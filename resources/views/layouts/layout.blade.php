<!DOCTYPE html>
<html>
<head>
    {{ Html::style('/bower_components/bootstrap/dist/css/bootstrap.css') }}
    {{ Html::style('/bower_components/bootstrap/dist/css/bootstrap-theme.css') }}
    {{ Html::style('/bower_components/bootstrap-treeview/dist/bootstrap-treeview.min.css') }}
    {{ Html::style('/css/style.css') }}

    {{ Html::style('/bower_components/jquery-treegrid/css/jquery.treegrid.css') }}
    {{ Html::style('/bower_components/select2/dist/css/select2.min.css') }}

    {{ Html::script('/bower_components/jquery/dist/jquery.min.js') }}

    {{ Html::style('/bower_components/iCheck/skins/flat/flat.css') }}
    {{ Html::script('/bower_components/iCheck/icheck.min.js') }}

    {{ Html::script('/bower_components/select2/dist/js/select2.full.js') }}

</head>
<body>

    <header>

    </header>
    {!! $nav !!}
    <section class="content">

    <div class="container">
        {!!  $content !!}

    </div>


    </section>
    <footer>

    </footer>
    {{ Html::script('/js/common.js') }}
    {{ Html::script('/js/jquery-ui.min.js') }}
    {{ Html::script('/js/treeView.js') }}
    {{ Html::script('/bower_components/bootstrap-treeview/dist/bootstrap-treeview.min.js') }}
    {{ Html::script('/bower_components/bootstrap/dist/js/bootstrap.min.js') }}
    {{ Html::script('/bower_components/jquery-treegrid/js/jquery.treegrid.min.js') }}
</body>
</html>
