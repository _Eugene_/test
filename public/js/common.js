$(document).ready(function () {

    var modalActive = 0;
    requirePlugins();

    $('section.content').delegate('form[name=filter]','submit', function (e) {
        e.preventDefault(e);
        getListData();
    });

    $('.table-wrapper').delegate('.list-navigate  a', 'click', function (e) {
        e.preventDefault(e);

        var page = $('input[name=page][type=hidden]');

        var pageNumeric = parseInt(page.val()) ? parseInt(page.val()) : 1;

        if ($(this).data('page').valueOf() == 'p1' || $(this).data('page').valueOf() == '-1') {

            page.val($(this).data('page') === 'p1' ? pageNumeric + 1 : pageNumeric -1 );

        } else {
            page.val($(this).data('page'));
        }

        getListData();
    });


    $('.table-wrapper').delegate('a.delete-employee', 'click', function (e) {
        e.preventDefault(e);
        $.ajax({
            method: "POST",
            url: "/edit/ajaxDelete",
            data: {'id': $(this).data('id')},
            success: function (data) {
                getListData();
            },
            error: function (data) {

            }
        });

    })

    function getListData() {
        var formData = new FormData($('form[name=filter]').get(0));
        formData.append('orderBy',JSON.stringify(orderBy));
        formData.append('selectBoss', modalActive);
        $.ajax({
            method: "post",
            url: "/list/ajaxList",
            data: formData,
            processData: false,
            contentType: false,
            success: function (data) {
                $('.table-wrapper').html(data);
            }
        });
    }

    $('.delete-avatar').click(function () {
        $(this).closest('.img-wrapper').find('img').attr('src', '');
        // let form = $(document.forms.profile);
        $('input[name=deleteImg][type=hidden]').val('deleteImg');
    });

    $('.edit-avatar').click(function (e) {
        $('input[type=file]').click();
    });

    $('input:file').change(function (e) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('.img-wrapper>img').attr('src', reader.result);
        }

        reader.readAsDataURL(this.files[0]);

    });

    $('#select-boss').on('shown.bs.modal', function () {
        modalActive = 1;
        $.ajax({
            url: '/list',
            method: 'get',
            data: {'selectBoss':1, 'id' : $('input[type=hidden][name=id]').val()},
            success: function (data) {
                $('#select-boss .modal-body .wrapper').html(data);
                requirePlugins();
            }

        });

        $(this).delegate('.accept-select-boss', 'click', function () {
            $('input[type=hidden][name=boss_id]').val($(this).data('id'));
            $('input[type=text][name=boss_name]').val($(this).data('name'));

            $('#select-boss').modal('hide');
        });
    })


    var orderBy = {};
    $('.table-wrapper').delegate('th', 'click', function(e){

        var header = $(this).children()[0];

        var ordered = 0;
        switch (parseInt($(header).data("order"))){
            case 0:
                ordered = 1;
                break;
            case  1:
                ordered = 2;
                break;
            default:
                ordered = 0;
                break;

        };
        $(header).data("order", ordered);
        orderBy[$(header).data('col-name')] = ordered;
        getListData();
    })

})


function requirePlugins() {
    $(".select2").select2({width: "100%"});
    $('input').iCheck({
        checkboxClass: 'icheckbox_flat',
        radioClass: 'iradio_flat'
    });
}

