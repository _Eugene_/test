
$(document).ready(function(){

//     // $.fn.extend({
//         // treed: function (o) {

//             // var openedClass = 'glyphicon-minus-sign';
//             // var closedClass = 'glyphicon-plus-sign';
//
//             // if (typeof o != 'undefined'){
//             //     if (typeof o.openedClass != 'undefined'){
//             //         openedClass = o.openedClass;
//             //     }
//             //     if (typeof o.closedClass != 'undefined'){
//             //         closedClass = o.closedClass;
//             //     }
//             // };
//
//             //initialize each of the top levels
//             // var tree = $(this);
//             // tree.addClass("tree");
//             // tree.find('li').has("ul").each(function () {
//             //     var branch = $(this); //li with children ul
//             //     branch.prepend("<i class='indicator glyphicon " + closedClass + "'></i>");
//             //     branch.addClass('branch');
//             //     branch.on('click', function (e) {
//             //         if (this == e.target) {
//             //             var icon = $(this).children('i:first');
//             //             icon.toggleClass(openedClass + " " + closedClass);
//             //             $(this).children().children().toggle();
//             //         }
//             //     })
//             //     branch.children().children().toggle();
//             // });
//             //fire event from the dynamically added icon
//             // tree.find('.branch .indicator').each(function(){
//             //     $(this).on('click', function () {
//             //         $(this).closest('li').click();
//             //     });
//             // });
//             //fire event to open branch if the li contains an anchor instead of text
//             // tree.find('.branch>a').each(function () {
//             //     $(this).on('click', function (e) {
//             //         var container = $(this).closest('li');
//             //         getChildren($(this).data('id'),container);
//             //         $(this).closest('li').click();
//             //         e.preventDefault();
//             //     });
//             // });
//             //fire event to open branch if the li contains a button instead of text
//             // tree.find('.branch>button').each(function () {
//             //     $(this).on('click', function (e) {
//             //
//             //         $(this).closest('li').click();
//             //         e.preventDefault();
//             //     });
//             // });
//         // }
//     // });
//
    $(".tree-wrapper").delegate("a", "click" ,function (e) {

        e.preventDefault(e);
        if($(this).data('cnt') > 0) {
            console.log($(this).closest('li').find('ul'));
            getChildren($(this).data('id'), $(this).closest("li"));
        }
    });


    function getChildren(id, container) {

        $.ajax({
                method: "POST",
                url: "/treeView/ajaxGetChildren",
                data: {"id": id},
                success: function (data) {
                    console.log(data);
                    $(container).find('ul').toggle().html(data);

                },
                error: function (data) {
                    $('body').html(data.responseText);
                }

            }
        );
    }

//
// $('.tree').treed({openedClass:'glyphicon-chevron-down', closedClass:'glyphicon-chevron-right'});
//
})