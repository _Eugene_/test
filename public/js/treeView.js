$(document).ready(function () {

    // $.fn.extend({
    //     treed: function (o) {
    //
    //         var openedClass = 'glyphicon-minus-sign';
    //         var closedClass = 'glyphicon-plus-sign';
    //
    //         if (typeof o != 'undefined'){
    //             if (typeof o.openedClass != 'undefined'){
    //                 openedClass = o.openedClass;
    //             }
    //             if (typeof o.closedClass != 'undefined'){
    //                 closedClass = o.closedClass;
    //             }
    //         };
    //
    //         // initialize each of the top levels
    //         var tree = $(this);
    //         tree.addClass("tree");
    //         tree.find('li').has("ul").each(function () {
    //             var branch = $(this); //li with children ul
    //             branch.prepend("<i class='indicator glyphicon " + closedClass + "'></i>");
    //             branch.addClass('branch');
    //             branch.on('click', function (e) {
    //                 if (this == e.target) {
    //                     var icon = $(this).children('i:first');
    //                     icon.toggleClass(openedClass + " " + closedClass);
    //                     $(this).children().children().toggle();
    //                 }
    //             })
    //             branch.children().children().toggle();
    //         });
    //         // fire event from the dynamically added icon
    //         tree.find('.branch .indicator').each(function(){
    //             $(this).on('click', function () {
    //                 $(this).closest('li').click();
    //             });
    //         });
    //         // fire event to open branch if the li contains an anchor instead of text
    //         tree.find('.branch>a').each(function () {
    //             $(this).on('click', function (e) {
    //                 var container = $(this).closest('li');
    //                 getChildren($(this).data('id'),container);
    //                 $(this).closest('li').click();
    //                 e.preventDefault();
    //             });
    //         });
    //         // fire event to open branch if the li contains a button instead of text
    //         tree.find('.branch>button').each(function () {
    //             $(this).on('click', function (e) {
    //
    //                 $(this).closest('li').click();
    //                 e.preventDefault();
    //             });
    //         });
    //     }
    // });
//
    $(".tree-wrapper").delegate(".tree li .item", "click", function (e) {

        e.preventDefault(e);
        var icon = $(this).closest('li').find('i').first();
        if (icon.hasClass('glyphicon-chevron-right')) {
            icon.removeClass('glyphicon-chevron-right').addClass('glyphicon-chevron-down');
        } else if (icon.hasClass('glyphicon-chevron-down')) {
            icon.removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-right');
        }
        // if($(this).closest('li').find('ul').hasClass('display-none')){
        $(this).closest('li').find('ul').first().toggleClass('display-none');
        // }


        if ($(this).find('a').data('cnt') > 0 && $(this).closest('li').find('ul').children().length == 0) {
            getChildren($(this).find('a').first().data('id'), this.closest('li'));
        }
    });


    function getChildren(id, container, add) {
        $.ajax({
                method: "POST",
                url: "/treeView/ajaxGetChildren",
                data: {"id": parseInt(id)},
                success: function (data) {
                    $(container).find('ul').first().html(data);
                    console.log('ajax');
                },
                error: function (data) {
                    $('body').html(data.responseText);
                }

            }
        );
    }

    var draggbleElement;

    $('.tree-wrapper').on('mouseover', 'div.item',
        function () {
            $(this).draggable(
                {
                    // axis: 'y',
                    cursor: 'move',
                    helper: "clone",
                    revert: true,
                    containment: ".tree-wrapper",
                    start: function (e, ui) {
                        ui.helper.addClass('drag-helper');
                        draggbleElement = this;
                        showHideDeleteBlock();
                    },
                    drag: function () {

                    },
                    stop: function () {


                        $('.drag-helper').remove();
                        showHideDeleteBlock();
                    }
                }
            );
        });


    $('.tree-wrapper').on('mouseover', '.delete-tree-item', function () {
        $(this).droppable({
            drop: function (e, ui) {
                var id = $(draggbleElement).find('a').data('id');
                var parentDraggable = $(draggbleElement).closest('li').closest('ul').parent();
                var parentId = $(parentDraggable).find('a').first().data('id');
                $.ajax({
                    url: '/treeView/ajaxDeleteItem',
                    method: 'POST',
                    data: {'id': id},
                    success: function (data) {
                        var parentLi = $(draggbleElement).closest('li');
                        // console.log(parseInt($(draggbleElement).find('a').first().data('cnt')));
                        if (parentLi.find('ul').first().children().length != 0) {
                            $(parentLi).closest('ul').first().prepend($(parentLi).find('ul').first().children());
                        } else if (parseInt($(draggbleElement).find('a').first().data('cnt')) != 0) {
                            $(parentLi).closest('ul').first().prepend(data);
                            // getChildren(parseInt($(draggbleElement).find('a').first().data('id')), , 1);
                        }

                        $(draggbleElement).closest('li').remove();
                        alert('удалено');
                    },
                    error: function (data) {
                        $('body').html(data.responseText);
                    }
                })


            }

        })


    });


    $('.tree-wrapper').on('mouseover', '.tree li .item', function () {
        $(this).droppable({
            drop: function (e, ui) {
                var id = $(draggbleElement).find('a').data('id');
                var parentDraggable = $(draggbleElement).closest('li').closest('ul').parent();
                var parentId = $(parentDraggable).find('a').first().data('id');
                var newBossId = $(this).find('a').data('id');
                var newBossItem = $(this);
                // $(draggbleElement).closest('li').remove();;


                if (id != newBossId) {
                    // var aCnt = $(parentDraggable).find('a').first().data('cnt')
                    // if(parseInt(aCnt) == 1 && !$(draggbleElement).closest('li').closest('ul').hasClass('tree')){
                    //     $(parentDraggable).find('i').first().remove();
                    // }
                    // console.log($(newBossItem).find('i').html());
                    // if(isNaN($(newBossItem).find('i'))){
                    //     $(newBossItem).prepend('<i class="indicator glyphicon glyphicon-chevron-right"></i>')
                    // }
                    //
                    // $(newBossItem).find('ul').first().append($(ui.draggable).parent())
                    $.ajax({
                        url: '/treeView/ajaxUpdateIerarchy',
                        method: 'POST',
                        data: {'id': id, 'newBoss': newBossId},
                        success: function (data) {
                            var aCnt = $(parentDraggable).find('a').first().data('cnt')
                            if (parseInt(aCnt) == 1 && !$(draggbleElement).closest('li').closest('ul').hasClass('tree')) {
                                $(parentDraggable).find('i').first().remove();
                            }

                            if ($(newBossItem).find('i').length == 0) {
                                $(newBossItem).prepend('<i class="indicator glyphicon glyphicon-chevron-right"></i>')
                            }

                            $(newBossItem).closest('li').find('ul').first().append('<li class="' + $(draggbleElement).closest('li').attr('class') + '">'+$(draggbleElement).closest('li').html() + '</li>');

                            $(draggbleElement).closest('li').remove();
                            var aCnt = $(parentDraggable).find('a').first().data('cnt');
                            if (parseInt(aCnt) == 1 && !$(draggbleElement).closest('li').closest('ul').hasClass('tree')) {
                                $(parentDraggable).find('a').first().data('cnt', 0);
                                $(parentDraggable).find('i').first().remove();
                            }

                            var cnt = parseInt($(newBossItem).find('a').data('cnt')) + 1;
                            // console.log(cnt);
                            $(newBossItem).find('a').data('cnt',cnt);
                            // console.log($(newBossItem).find('a').data() );
                            // console.log($(newBossItem).find('a').html() );
                            $('.ui-draggable-dragging').remove();

                        },
                        error: function () {
                            alert('error');
                        }
                    })
                }


            }

        })


    })

    function createRemoveBranch() {

    }

    // $('.delete-tree-item')
    // });

    function showHideDeleteBlock() {
        $('.delete-tree-item').toggleClass('display-none');
    }


//
// $(".tree").treed({openedClass:'glyphicon-chevron-down', closedClass:'glyphicon-chevron-right'});
//
})